
#ifdef RCT_NEW_ARCH_ENABLED
#import "RNNeEuclidSpec.h"

@interface NeEuclid : NSObject <NativeNeEuclidSpec>
#else
#import <React/RCTBridgeModule.h>

@interface NeEuclid : NSObject <RCTBridgeModule>
#endif

@end
