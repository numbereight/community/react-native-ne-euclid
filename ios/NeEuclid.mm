#import "NeEuclid.h"
@import Audiences;

@implementation NeEuclid
RCT_EXPORT_MODULE()

-(void)startRecording:(NSString*)apiKey
         withResolver:(RCTPromiseResolveBlock)resolve
         withRejecter:(RCTPromiseRejectBlock)reject {
    NEXAPIToken *token = [NEXNumberEight startWithApiKey:apiKey
                                           launchOptions:nil
                                              completion:nil];
    if (token != nil) {
        [NEXAudiences startRecordingWithApiToken:token];
    }

    resolve(); // TODO put in onStart callback
}

-(void)stopRecording {
    [NEXAudiences stopRecording];
    [NEXNumberEight stop];
}

-(void)currentMemberships:(RCTPromiseResolveBlock)resolve
             withRejecter:(RCTPromiseRejectBlock)reject {
    NSString* audiences = [[NEXAudiences currentMemberships] serialize];
    resolve(audiences);
}

// Don't compile this code when we build for the old architecture.
#ifdef RCT_NEW_ARCH_ENABLED
- (std::shared_ptr<facebook::react::TurboModule>)getTurboModule:
    (const facebook::react::ObjCTurboModule::InitParams &)params
{
    return std::make_shared<facebook::react::NativeNeEuclidSpecJSI>(params);
}
#endif

@end
